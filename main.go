package main

import (
	"fmt"
	"github.com/beevik/ntp"
	"os"
)

func main() {
	resp, err := ntp.Time("0.beevik-ntp.pool.ntp.org")
	if err != nil {
		fmt.Fprintf(os.Stderr, "error message: %s", err)
		os.Exit(1)
	}
	fmt.Println(resp)
}
