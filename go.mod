module gitlab.com/otus-go-hw/hello-now

go 1.13

require (
	github.com/beevik/ntp v0.2.0
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	golang.org/x/sys v0.0.0-20200117145432-59e60aa80a0c // indirect
)
